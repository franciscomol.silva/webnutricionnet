﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebNutricionNet.Models
{
    public class Paciente
    {
        public int Id { get; set; }

        [Required]
        public int Rut { get; set; }
        [StringLength(250)]
        public string Nombre { get; set; }
        [StringLength(250)]
        public string Apellidos { get; set; }
        public DateTime FechaNacimiento { get; set; }
        [StringLength(250)]
        public string Correo { get; set; }
        [StringLength(20)]
        public string Telefono { get; set; }
        public string LugarAtencion { get; set; }
    }
}