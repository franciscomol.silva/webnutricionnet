﻿using System.Collections.Generic;
using System.Linq;
using WebNutricionNet.Models;

namespace WebNutricionNet.Services
{
    public class ServicePaciente
    {
        public bool RegistroCliente(Models.ViewModel.Paciente viewPaciente)
        {
            bool isSucces = false;

            var paciente = new Paciente()
            {
                Rut = viewPaciente.Rut,
                Nombre = viewPaciente.Nombre,
                Apellidos = viewPaciente.Apellidos,
                FechaNacimiento = viewPaciente.FechaNacimiento.Value,
                Correo = viewPaciente.Correo,
                Telefono = viewPaciente.Telefono
            };

            try
            {
                using (var context = new NutricionContext())
                {
                    context.Pacientes.Add(paciente);
                    context.SaveChanges();
                    isSucces = true;
                }
            }
            catch
            {

            }


            return isSucces;
        }

        public Models.ViewModel.Pacientes RetornaPacientes()
        {
            var pacientes = new Models.ViewModel.Pacientes();
            var listPacientes = new List<Models.ViewModel.Paciente>();

            using (var context = new NutricionContext())
            {
                var dbPacientes = context.Pacientes.ToList();

                foreach (var p in dbPacientes)
                {
                    var aux = new Models.ViewModel.Paciente()
                    {
                        Id = p.Id,
                        Rut = p.Rut,
                        Nombre = p.Nombre,
                        Apellidos = p.Apellidos,
                        Correo = p.Correo,
                        FechaNacimiento = p.FechaNacimiento,
                        Telefono = p.Telefono,
                        LugarAtencion = p.LugarAtencion
                    };

                    listPacientes.Add(aux);
                }
            }

            pacientes.DatosPaciente = listPacientes;

            return pacientes;
        }
    }
}