﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebNutricionNet.Models
{
    public class NutricionContext : DbContext
    {
        public NutricionContext() : base("connectionSalud")
        {
        }

        public DbSet<Paciente> Pacientes { get; set; }

    }
}