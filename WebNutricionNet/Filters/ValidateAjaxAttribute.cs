﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace WebNutricionNet.Filters
{
    public class ValidateAjaxAttribute : ActionFilterAttribute
    {
        private int _wrapperStatusCode = 0;
        private JsonResult _wrapperData;

        public int StatusCode { get => _wrapperStatusCode; }
        public JsonResult Data { get => _wrapperData; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if (!filterContext.HttpContext.Request.IsAjaxRequest())
            //    return;

            var modelState = filterContext.Controller.ViewData.ModelState;
            if (!modelState.IsValid)
            {
                var errorModel =
                        from x in modelState.Keys
                        where modelState[x].Errors.Count > 0
                        select new
                        {
                            key = x,
                            errors = modelState[x].Errors.
                                                          Select(y => y.ErrorMessage).
                                                          ToArray()
                        };

                //Wrapper hecho para pruebas unitarias, no manipular
                _wrapperData = new JsonResult()
                {
                    Data = errorModel
                };

                filterContext.Result = _wrapperData;

                //Wrapper hecho para pruebas unitarias, no manipular
                _wrapperStatusCode = (int)HttpStatusCode.BadRequest;

                filterContext.HttpContext.Response.StatusCode = _wrapperStatusCode;
            }
        }
    }
}