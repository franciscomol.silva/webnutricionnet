﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebNutricionNet.Models.ViewModel
{
    public class Paciente
    {
        public int Id { get; set; }

        [Required]
        public int Rut { get; set; }

        [Required]
        [StringLength(250)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(250)]
        public string Apellidos { get; set; }
        [Required(ErrorMessage = "Fecha de Nacimiento es un campo requerido")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Fecha Nacimiento")]
        public DateTime? FechaNacimiento { get; set; }    
        [EmailAddress]
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string LugarAtencion { get; set; }
    }
}