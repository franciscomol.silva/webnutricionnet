﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebNutricionNet.Filters;
using WebNutricionNet.Models.ViewModel;
using WebNutricionNet.Services;

namespace WebNutricionNet.Controllers
{
    public class IngresoPacienteController : Controller
    {
        // GET: IngresoPaciente
        public ActionResult Index()
        {
            return View();
        }

        // GET: IngresoPaciente/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: IngresoPaciente/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IngresoPaciente/Create
        [ValidateAjax]
        [HttpPost]
        public ActionResult Create(Paciente paciente)
        {
            try
            {
                ServicePaciente service = new ServicePaciente();

                if(service.RegistroCliente(paciente))
                {
                    //Le retornamos a la vista a donde debe rediccionar
                    return Json(new { redirectToUrl = Url.Action("MenuPrincipal", "Menu") });
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: IngresoPaciente/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: IngresoPaciente/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: IngresoPaciente/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: IngresoPaciente/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
