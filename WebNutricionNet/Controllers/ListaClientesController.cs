﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebNutricionNet.Services;

namespace WebNutricionNet.Controllers
{
    public class ListaClientesController : Controller
    {
        // GET: ListaClientes
        public ActionResult Index()
        {
            ServicePaciente service = new ServicePaciente();

            var pacientes = service.RetornaPacientes();

            return View(pacientes);
        }

        // GET: ListaClientes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: ListaClientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ListaClientes/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ListaClientes/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ListaClientes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ListaClientes/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ListaClientes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
